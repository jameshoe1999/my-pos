-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2020 at 05:58 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_pos_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `invoice_id` int(11) UNSIGNED NOT NULL,
  `invoice_code` varchar(20) DEFAULT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `invoice_sub_total` double(10,3) NOT NULL DEFAULT 0.000,
  `invoice_discount` double(10,3) NOT NULL DEFAULT 0.000,
  `invoice_rounding` double(10,3) NOT NULL DEFAULT 0.000,
  `invoice_amount` double(10,3) NOT NULL DEFAULT 0.000,
  `invoice_remark` varchar(100) DEFAULT NULL,
  `invoice_status` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`invoice_id`, `invoice_code`, `user_id`, `invoice_sub_total`, `invoice_discount`, `invoice_rounding`, `invoice_amount`, `invoice_remark`, `invoice_status`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'INV200200001', 1, 0.000, 0.000, 0.000, 17.400, 'Testing invoice', 1, '2020-02-06 16:35:20', '2020-02-07 12:24:07', NULL, '', NULL, NULL),
(2, 'INV200200002', 1, 0.000, 0.000, 0.000, 2.300, 'Testing invoice', 1, '2020-02-06 17:41:17', '2020-02-06 17:45:33', NULL, '', NULL, NULL),
(3, 'INV200200003', 1, 0.000, 0.000, 0.000, 4.000, 'SALE', 1, '2020-02-06 17:54:13', '2020-02-06 17:54:13', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `invoices_v`
-- (See below for the actual view)
--
CREATE TABLE `invoices_v` (
`invoice_id` int(11) unsigned
,`invoice_code` varchar(20)
,`invoice_user` varchar(50)
,`invoice_sub_total` double(10,3)
,`invoice_discount` double(10,3)
,`invoice_rounding` double(10,3)
,`invoice_amount` double(10,3)
,`invoice_remark` varchar(100)
,`invoice_status` int(1) unsigned
,`created_at` timestamp
,`created_by` varchar(30)
);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `invoice_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_price` double(10,3) NOT NULL,
  `item_cost` double(10,3) NOT NULL,
  `item_sub_total` double(10,3) NOT NULL,
  `item_remark` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_id`, `product_id`, `item_qty`, `item_price`, `item_cost`, `item_sub_total`, `item_remark`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 1, 5, 2.000, 1.440, 10.000, 'Testing remark', '2020-02-06 17:14:30', '2020-02-06 17:14:47', NULL, '', NULL, NULL),
(2, 1, 1, 4, 1.850, 1.350, 7.400, NULL, '2020-02-06 17:14:30', '2020-02-06 17:14:51', NULL, '', NULL, NULL),
(5, 2, 1, 1, 2.300, 1.440, 2.300, 'Testing remark', '2020-02-06 17:50:44', NULL, NULL, '', NULL, NULL),
(6, 3, 1, 2, 2.000, 1.440, 4.000, 'Testing remark', '2020-02-06 17:54:13', NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_discounts`
--

CREATE TABLE `invoice_discounts` (
  `discount_id` int(11) UNSIGNED NOT NULL,
  `discount_code` varchar(30) NOT NULL,
  `invoice_id` int(11) UNSIGNED NOT NULL,
  `discount_type` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `discount_amount` double(10,3) NOT NULL,
  `discount_remark` varchar(100) DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `permissions_v`
-- (See below for the actual view)
--
CREATE TABLE `permissions_v` (
`permission_id` int(11) unsigned
,`permission_name` varchar(30)
,`view_access` tinyint(4)
,`create_access` tinyint(4)
,`edit_access` tinyint(4)
,`delete_access` tinyint(4)
,`print_access` tinyint(4)
,`permission_level` varchar(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `point_of_sales`
--

CREATE TABLE `point_of_sales` (
  `sale_id` int(11) UNSIGNED NOT NULL,
  `sale_code` varchar(20) NOT NULL,
  `customer_id` int(11) UNSIGNED NOT NULL,
  `salesperson` int(11) UNSIGNED NOT NULL,
  `sale_sub_total` double(10,3) NOT NULL,
  `sale_discount` double(10,3) NOT NULL DEFAULT 0.000,
  `sale_rounding` double(10,3) NOT NULL DEFAULT 0.000,
  `sale_amount` double(10,3) NOT NULL,
  `sale_remark` varchar(100) DEFAULT NULL,
  `sale_status` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `approved_by` varchar(30) DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(25) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `product_description` varchar(100) DEFAULT NULL,
  `product_brand` int(5) UNSIGNED NOT NULL,
  `product_category` int(5) UNSIGNED NOT NULL,
  `product_uom` int(5) UNSIGNED NOT NULL,
  `product_status` tinyint(1) NOT NULL DEFAULT 1,
  `effective_date_start` date DEFAULT NULL,
  `effective_date_end` date DEFAULT '2100-12-31',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `deleted_by` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_code`, `product_name`, `product_description`, `product_brand`, `product_category`, `product_uom`, `product_status`, `effective_date_start`, `effective_date_end`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'IPBZ4G', 'IP 4G/4S BUZZER', 'Apple iPhone 4G/4s buzzer', 1, 1, 1, 1, '2020-02-05', '2100-12-31', '2020-02-05 11:45:51', NULL, NULL, 'JamesHoe', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `products_v`
-- (See below for the actual view)
--
CREATE TABLE `products_v` (
`product_id` int(11) unsigned
,`product_name` varchar(50)
,`product_code` varchar(25)
,`product_description` varchar(100)
,`product_brand` varchar(25)
,`product_category` varchar(25)
,`product_uom` varchar(25)
,`product_status` tinyint(1)
,`effective_date_start` date
,`effective_date_end` date
);

-- --------------------------------------------------------

--
-- Table structure for table `product_adjustments`
--

CREATE TABLE `product_adjustments` (
  `adjustment_id` int(11) UNSIGNED NOT NULL,
  `adjustment_code` varchar(20) DEFAULT NULL,
  `adjustment_remark` varchar(100) DEFAULT NULL,
  `adjustment_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_brands`
--

CREATE TABLE `product_brands` (
  `brand_id` int(11) UNSIGNED NOT NULL,
  `brand_name` varchar(25) NOT NULL,
  `brand_description` varchar(50) DEFAULT NULL,
  `brand_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_brands`
--

INSERT INTO `product_brands` (`brand_id`, `brand_name`, `brand_description`, `brand_status`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Apple', NULL, 1, '2020-02-05 11:23:27', NULL, NULL, 'JamesHoe', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `category_name` varchar(25) NOT NULL,
  `category_description` varchar(50) DEFAULT NULL,
  `category_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`category_id`, `category_name`, `category_description`, `category_status`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'BUZZER', NULL, 1, '2020-02-05 11:28:26', NULL, NULL, 'JamesHoe', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_colors`
--

CREATE TABLE `product_colors` (
  `color_id` int(5) UNSIGNED NOT NULL,
  `color_name` varchar(25) NOT NULL,
  `color_code` varchar(7) NOT NULL,
  `color_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_colors`
--

INSERT INTO `product_colors` (`color_id`, `color_name`, `color_code`, `color_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Default', '#fff', 1, '2020-02-05 15:56:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_transactions`
--

CREATE TABLE `product_transactions` (
  `transaction_id` int(11) UNSIGNED NOT NULL,
  `transaction_code` varchar(20) DEFAULT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `transaction_qty` int(11) NOT NULL,
  `transaction_remark` varchar(50) NOT NULL,
  `transaction_status` tinyint(1) NOT NULL DEFAULT 1,
  `transaction_type` int(1) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `type_id` int(5) UNSIGNED NOT NULL,
  `type_name` varchar(25) NOT NULL,
  `type_status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`type_id`, `type_name`, `type_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Default', 1, '2020-02-05 15:14:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_uom`
--

CREATE TABLE `product_uom` (
  `unit_id` int(5) UNSIGNED NOT NULL,
  `unit_name` varchar(25) NOT NULL,
  `unit_code` varchar(25) NOT NULL,
  `unit_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_uom`
--

INSERT INTO `product_uom` (`unit_id`, `unit_name`, `unit_code`, `unit_status`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Unit', 'UNIT', 1, '2020-02-05 11:42:14', NULL, NULL, 'JamesHoe', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(25) NOT NULL,
  `product_color` int(5) UNSIGNED NOT NULL,
  `product_type` int(5) UNSIGNED NOT NULL,
  `product_cost` double(10,3) NOT NULL,
  `product_price` double(10,3) NOT NULL,
  `initial_qty` int(11) UNSIGNED NOT NULL,
  `current_qty` int(11) NOT NULL,
  `product_status` tinyint(1) NOT NULL DEFAULT 1,
  `effective_date_start` date DEFAULT NULL,
  `effective_date_end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `product_id`, `product_code`, `product_color`, `product_type`, `product_cost`, `product_price`, `initial_qty`, `current_qty`, `product_status`, `effective_date_start`, `effective_date_end`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 'IPBZ4GDF', 1, 1, 1.440, 2.000, 10, 0, 1, '2020-02-05', NULL, '2020-02-05 15:57:00', '2020-02-07 12:46:12', NULL, 'JamesHoe', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `permission_id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL,
  `permission_name` varchar(30) NOT NULL,
  `view_access` tinyint(1) NOT NULL DEFAULT 0,
  `create_access` tinyint(1) NOT NULL DEFAULT 0,
  `edit_access` tinyint(1) NOT NULL DEFAULT 0,
  `delete_access` tinyint(1) NOT NULL DEFAULT 0,
  `print_access` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`permission_id`, `role_id`, `permission_name`, `view_access`, `create_access`, `edit_access`, `delete_access`, `print_access`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'users', 1, 1, 1, 1, 1, '2020-02-07 15:27:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_records`
--

CREATE TABLE `sale_records` (
  `record_id` int(11) UNSIGNED NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `sale_qty` int(5) NOT NULL,
  `sale_price` double(10,3) UNSIGNED NOT NULL,
  `sale_cost` double(10,3) UNSIGNED NOT NULL,
  `sale_sub_total` double(10,3) UNSIGNED NOT NULL,
  `sale_discount` double(10,3) UNSIGNED DEFAULT 0.000,
  `sale_rounding` double(5,3) DEFAULT 0.000,
  `sale_remark` varchar(50) DEFAULT NULL,
  `sale_status` tinyint(2) NOT NULL DEFAULT 1,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_first_name` varchar(30) NOT NULL,
  `user_last_name` varchar(30) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` text NOT NULL,
  `user_confirmed_password` tinyint(1) NOT NULL DEFAULT 0,
  `user_mobile_no` varchar(11) NOT NULL,
  `user_designation` varchar(30) NOT NULL DEFAULT 'User',
  `user_role` int(3) UNSIGNED NOT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT 0,
  `address_1` varchar(50) DEFAULT NULL,
  `address_2` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_first_name`, `user_last_name`, `user_email`, `user_password`, `user_confirmed_password`, `user_mobile_no`, `user_designation`, `user_role`, `user_status`, `address_1`, `address_2`, `city`, `state`, `country`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'JamesHoe', 'Jin Xin', 'Hoe', 'jameshoe@example.com', 'af8457cc606863384ccda49bdce53671aaa5f068', 0, '01139004007', 'Software Developer', 1, 1, NULL, NULL, NULL, NULL, NULL, '2020-02-04 17:31:05', '2020-02-05 03:17:01', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_credits`
--

CREATE TABLE `user_credits` (
  `credit_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `credit_amount` double(10,3) NOT NULL DEFAULT 0.000,
  `credit_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(30) NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `deleted_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_credits`
--

INSERT INTO `user_credits` (`credit_id`, `user_id`, `credit_amount`, `credit_status`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 1, 0.000, 1, '2020-02-06 14:48:02', NULL, NULL, 'JamesHoe', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `permission_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `permission_name` varchar(30) NOT NULL,
  `view_access` tinyint(1) NOT NULL DEFAULT 0,
  `create_access` tinyint(1) NOT NULL DEFAULT 0,
  `edit_access` tinyint(1) NOT NULL DEFAULT 0,
  `delete_access` tinyint(1) NOT NULL DEFAULT 0,
  `print_access` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`permission_id`, `user_id`, `permission_name`, `view_access`, `create_access`, `edit_access`, `delete_access`, `print_access`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'users', 1, 1, 1, 0, 0, '2020-02-07 15:27:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `role_id` int(3) UNSIGNED NOT NULL,
  `role_name` varchar(20) NOT NULL,
  `role_status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`role_id`, `role_name`, `role_status`, `created_at`, `created_by`) VALUES
(1, 'Admin', 1, '2020-02-05 03:16:49', 'James'),
(2, 'Staff', 1, '2020-02-05 10:54:48', 'JamesHoe');

-- --------------------------------------------------------

--
-- Stand-in structure for view `variants_v`
-- (See below for the actual view)
--
CREATE TABLE `variants_v` (
`id` int(11) unsigned
,`product_id` int(11) unsigned
,`product_name` varchar(50)
,`product_code` varchar(25)
,`product_color` varchar(25)
,`color_code` varchar(7)
,`product_type` varchar(25)
,`product_cost` double(10,3)
,`product_price` double(10,3)
,`current_qty` int(11)
,`product_uom` varchar(25)
,`product_status` tinyint(1)
,`effective_date_start` date
,`effective_date_end` date
);

-- --------------------------------------------------------

--
-- Structure for view `invoices_v`
--
DROP TABLE IF EXISTS `invoices_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `invoices_v`  AS  select `i`.`invoice_id` AS `invoice_id`,`i`.`invoice_code` AS `invoice_code`,`u`.`user_name` AS `invoice_user`,`i`.`invoice_sub_total` AS `invoice_sub_total`,`i`.`invoice_discount` AS `invoice_discount`,`i`.`invoice_rounding` AS `invoice_rounding`,`i`.`invoice_amount` AS `invoice_amount`,`i`.`invoice_remark` AS `invoice_remark`,`i`.`invoice_status` AS `invoice_status`,`i`.`created_at` AS `created_at`,`i`.`created_by` AS `created_by` from (`invoices` `i` left join `users` `u` on(`i`.`user_id` = `u`.`user_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `permissions_v`
--
DROP TABLE IF EXISTS `permissions_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `permissions_v`  AS  select `permission`.`permission_id` AS `permission_id`,`permission`.`permission_name` AS `permission_name`,`permission`.`view_access` AS `view_access`,`permission`.`create_access` AS `create_access`,`permission`.`edit_access` AS `edit_access`,`permission`.`delete_access` AS `delete_access`,`permission`.`print_access` AS `print_access`,`permission`.`permission_level` AS `permission_level` from (select `user_permissions`.`permission_id` AS `permission_id`,`user_permissions`.`permission_name` AS `permission_name`,`user_permissions`.`view_access` AS `view_access`,`user_permissions`.`create_access` AS `create_access`,`user_permissions`.`edit_access` AS `edit_access`,`user_permissions`.`delete_access` AS `delete_access`,`user_permissions`.`print_access` AS `print_access`,'1' AS `permission_level` from `user_permissions` union all select `role_permissions`.`permission_id` AS `permission_id`,`role_permissions`.`permission_name` AS `permission_name`,`role_permissions`.`view_access` AS `view_access`,`role_permissions`.`create_access` AS `create_access`,`role_permissions`.`edit_access` AS `edit_access`,`role_permissions`.`delete_access` AS `delete_access`,`role_permissions`.`print_access` AS `print_access`,'2' AS `permission_level` from `role_permissions`) `permission` group by `permission`.`permission_name` order by `permission`.`permission_level` ;

-- --------------------------------------------------------

--
-- Structure for view `products_v`
--
DROP TABLE IF EXISTS `products_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `products_v`  AS  select `p`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`p`.`product_code` AS `product_code`,`p`.`product_description` AS `product_description`,`pb`.`brand_name` AS `product_brand`,`pc`.`category_name` AS `product_category`,`uom`.`unit_name` AS `product_uom`,`p`.`product_status` AS `product_status`,`p`.`effective_date_start` AS `effective_date_start`,`p`.`effective_date_end` AS `effective_date_end` from (((`products` `p` left join `product_brands` `pb` on(`p`.`product_brand` = `pb`.`brand_id`)) left join `product_categories` `pc` on(`p`.`product_category` = `pc`.`category_id`)) left join `product_uom` `uom` on(`p`.`product_uom` = `uom`.`unit_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `variants_v`
--
DROP TABLE IF EXISTS `variants_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `variants_v`  AS  select `pv`.`id` AS `id`,`pv`.`product_id` AS `product_id`,`p`.`product_name` AS `product_name`,`pv`.`product_code` AS `product_code`,`pc`.`color_name` AS `product_color`,`pc`.`color_code` AS `color_code`,`pt`.`type_name` AS `product_type`,`pv`.`product_cost` AS `product_cost`,`pv`.`product_price` AS `product_price`,`pv`.`current_qty` AS `current_qty`,`uom`.`unit_name` AS `product_uom`,`pv`.`product_status` AS `product_status`,`pv`.`effective_date_start` AS `effective_date_start`,`pv`.`effective_date_end` AS `effective_date_end` from ((((`product_variants` `pv` left join `products` `p` on(`pv`.`product_id` = `p`.`product_id`)) left join `product_types` `pt` on(`pv`.`product_type` = `pt`.`type_id`)) left join `product_colors` `pc` on(`pv`.`product_color` = `pc`.`color_id`)) left join `product_uom` `uom` on(`p`.`product_uom` = `uom`.`unit_id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`invoice_id`),
  ADD UNIQUE KEY `invoice_code` (`invoice_code`),
  ADD KEY `DEALER` (`user_id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ITEM` (`product_id`),
  ADD KEY `INVOICE` (`invoice_id`);

--
-- Indexes for table `invoice_discounts`
--
ALTER TABLE `invoice_discounts`
  ADD PRIMARY KEY (`discount_id`),
  ADD UNIQUE KEY `discount_code` (`discount_code`),
  ADD KEY `DISCOUNT` (`invoice_id`);

--
-- Indexes for table `point_of_sales`
--
ALTER TABLE `point_of_sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD KEY `CUSTOMER` (`customer_id`),
  ADD KEY `SALESPERSON` (`salesperson`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `UNIQUE` (`product_code`),
  ADD KEY `BRAND` (`product_brand`),
  ADD KEY `CATEGORY` (`product_category`),
  ADD KEY `UNIT` (`product_uom`);

--
-- Indexes for table `product_adjustments`
--
ALTER TABLE `product_adjustments`
  ADD PRIMARY KEY (`adjustment_id`),
  ADD UNIQUE KEY `adjustment_code` (`adjustment_code`);

--
-- Indexes for table `product_brands`
--
ALTER TABLE `product_brands`
  ADD PRIMARY KEY (`brand_id`),
  ADD UNIQUE KEY `NAME` (`brand_name`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `NAME` (`category_name`);

--
-- Indexes for table `product_colors`
--
ALTER TABLE `product_colors`
  ADD PRIMARY KEY (`color_id`),
  ADD UNIQUE KEY `color_name` (`color_name`,`color_code`);

--
-- Indexes for table `product_transactions`
--
ALTER TABLE `product_transactions`
  ADD PRIMARY KEY (`transaction_id`),
  ADD UNIQUE KEY `transaction_code` (`transaction_code`),
  ADD KEY `PRODUCT` (`product_id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`type_id`),
  ADD UNIQUE KEY `NAME` (`type_name`);

--
-- Indexes for table `product_uom`
--
ALTER TABLE `product_uom`
  ADD PRIMARY KEY (`unit_id`),
  ADD UNIQUE KEY `CODE` (`unit_code`),
  ADD KEY `NAME` (`unit_name`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `CODE` (`product_code`),
  ADD KEY `TYPE` (`product_type`),
  ADD KEY `COLOR` (`product_color`),
  ADD KEY `PARENT` (`product_id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD UNIQUE KEY `permission_name` (`permission_name`),
  ADD KEY `PERMISSION_ROLE` (`role_id`),
  ADD KEY `permission_name_2` (`permission_name`);

--
-- Indexes for table `sale_records`
--
ALTER TABLE `sale_records`
  ADD PRIMARY KEY (`record_id`),
  ADD KEY `sale_id` (`sale_id`,`product_id`),
  ADD KEY `SALE_ITEM` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UNIQUE` (`user_name`);

--
-- Indexes for table `user_credits`
--
ALTER TABLE `user_credits`
  ADD PRIMARY KEY (`credit_id`),
  ADD KEY `USERS` (`user_id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD UNIQUE KEY `permission_name` (`permission_name`),
  ADD KEY `PERMISSION_USER` (`user_id`),
  ADD KEY `permission_name_2` (`permission_name`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `invoice_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `invoice_discounts`
--
ALTER TABLE `invoice_discounts`
  MODIFY `discount_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `point_of_sales`
--
ALTER TABLE `point_of_sales`
  MODIFY `sale_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_adjustments`
--
ALTER TABLE `product_adjustments`
  MODIFY `adjustment_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_brands`
--
ALTER TABLE `product_brands`
  MODIFY `brand_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_colors`
--
ALTER TABLE `product_colors`
  MODIFY `color_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_transactions`
--
ALTER TABLE `product_transactions`
  MODIFY `transaction_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `type_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_uom`
--
ALTER TABLE `product_uom`
  MODIFY `unit_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `permission_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sale_records`
--
ALTER TABLE `sale_records`
  MODIFY `record_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_credits`
--
ALTER TABLE `user_credits`
  MODIFY `credit_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `permission_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `role_id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `DEALER` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD CONSTRAINT `INVOICE` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`invoice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ITEM` FOREIGN KEY (`product_id`) REFERENCES `product_variants` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `invoice_discounts`
--
ALTER TABLE `invoice_discounts`
  ADD CONSTRAINT `DISCOUNT` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`invoice_id`) ON UPDATE CASCADE;

--
-- Constraints for table `point_of_sales`
--
ALTER TABLE `point_of_sales`
  ADD CONSTRAINT `CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `SALESPERSON` FOREIGN KEY (`salesperson`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `BRAND` FOREIGN KEY (`product_brand`) REFERENCES `product_brands` (`brand_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `CATEGORY` FOREIGN KEY (`product_category`) REFERENCES `product_categories` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `UNIT` FOREIGN KEY (`product_uom`) REFERENCES `product_uom` (`unit_id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_transactions`
--
ALTER TABLE `product_transactions`
  ADD CONSTRAINT `PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `product_variants` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD CONSTRAINT `COLOR` FOREIGN KEY (`product_color`) REFERENCES `product_colors` (`color_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `PARENT` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TYPE` FOREIGN KEY (`product_type`) REFERENCES `product_types` (`type_id`) ON UPDATE CASCADE;

--
-- Constraints for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD CONSTRAINT `PERMISSION_ROLE` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`role_id`) ON UPDATE CASCADE;

--
-- Constraints for table `sale_records`
--
ALTER TABLE `sale_records`
  ADD CONSTRAINT `SALE` FOREIGN KEY (`sale_id`) REFERENCES `point_of_sales` (`sale_id`),
  ADD CONSTRAINT `SALE_ITEM` FOREIGN KEY (`product_id`) REFERENCES `product_variants` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `USER_ROLE` FOREIGN KEY (`user_role`) REFERENCES `user_roles` (`role_id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_credits`
--
ALTER TABLE `user_credits`
  ADD CONSTRAINT `USERS` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD CONSTRAINT `PERMISSION_USER` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
