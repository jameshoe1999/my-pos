<?php

if ( ! function_exists('show_404'))
{
	/**
	 * 404 Page Handler
	 *
	 * This function is similar to the show_error() function above
	 * However, instead of the standard error template it displays
	 * 404 errors.
	 *
	 * @param	string
	 * @param	bool
	 * @return	void
	 */
	function show_404($page = '', $log_error = TRUE)
	{
        $rest =& load_class('REST_Controller', 'libraries');
        $rest->response(['error' => 'Content not found.', 'status' => false], 404);
		exit(4); // EXIT_UNKNOWN_FILE
	}
}