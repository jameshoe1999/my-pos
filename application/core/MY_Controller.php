<?php

use Restserver\Libraries\REST_Controller;

require APPPATH . 'libraries/REST_Controller.php';
/**
 * A base controller for CodeIgniter with view autoloading, layout support,
 * model loading, helper loading, asides/partials and per-controller 404
 *
 * @link http://github.com/jamierumbelow/codeigniter-base-controller
 * @copyright Copyright (c) 2012, Jamie Rumbelow <http://jamierumbelow.net>
 */

class MY_Controller extends REST_Controller
{

  /* --------------------------------------------------------------
    * VARIABLES
    * ------------------------------------------------------------ */

  /**
   * A list of models to be auto-loaded
   */
  protected $models = array();

  /**
   * A formatting string for the model autoloading feature.
   * The percent symbol (%) will be replaced with the model name.
   */
  protected $model_string = '%Model';

  /**
   * A list of helpers to be autoloaded
   */
  protected $helpers = array();

  protected $require_auth = true;

  protected $default_model = '';

  protected $user;

  protected $fields = array();

  /* --------------------------------------------------------------
    * GENERIC METHODS
    * ------------------------------------------------------------ */

  /**
   * Initialize the controller, tie into the CodeIgniter super-object
   * and try to autoload the models and helpers
   */
  public function __construct()
  {
    parent::__construct();

    if (strpos($_SERVER['REQUEST_URI'], 'api')) { // skipped token validation
      $this->user = $this->verify_request();
      if (!$this->user) {
        show_error(NULL, parent::HTTP_UNAUTHORIZED);
      }
      return;
    }
  }

  /* --------------------------------------------------------------
    * AUTHENTICATION WITH JWT FOR EVERY REQUEST
    * ------------------------------------------------------------ */
  protected function verify_request()
  {
    if (!$this->require_auth) {
      return true;
    }
    $headers = $this->input->request_headers();
    $token = $headers['Authorization'] ?? NULL;

    try {
      $data = AUTHORIZATION::validate_token($token);
      if (!$data) {
        throw new Exception('Unauthorized action.');
      } else {
        $user = $this->users->get_by('user_name', $data->username);
        return $user;
      }
    } catch (Exception $e) {
      $this->response(['error' => $e->getMessage(), 'status' => false], parent::HTTP_UNAUTHORIZED);
    }
  }

  /* --------------------------------------------------------------
    * ROUTE AUTHORIZATION CHECKING
    * ------------------------------------------------------------ */
  public function return_404()
  {
    show_error(NULL, parent::HTTP_NOT_FOUND);
  }

  public function return_success($data = null, $code = parent::HTTP_OK, $action = NO_ACTION)
  {
    $response = array('status' => true);
    switch ($action)
    {
      case CREATE:
        $response['message'] = 'Created successfully.';
      break;
      case UPDATE:
        $response['message'] = 'Updated successfully.';
      break;
      case DELETE:
        $response['message'] = 'Removed successfully.';
      break;
      default:
        $response['message'] = 'Requested successfully';
    }
    
    if ($data) {
      $response['data'] = $data;
    }
    
    return $this->response($response, $code);
  }

  public function return_error($message = NULL, $code = parent::HTTP_INTERNAL_SERVER_ERROR)
  {
      if (!$message) {
          switch ($code)
          {
              case parent::HTTP_BAD_REQUEST:
                  $message = $this->form_validation->error_array() ?? 'Bad request.';
              break;
              case parent::HTTP_UNAUTHORIZED:
                  $message = 'Unauthorized access.';
              break;
              case parent::HTTP_FORBIDDEN:
                  $message = 'Unauthorized access.';
              break;
              case parent::HTTP_NOT_FOUND:
                  $message = 'No record found.';
              default:
                  $message = 'Internal server error. Kindly contact system admin.';
              break;
          }
      }
      
      return $this->response(['error'=> $message, 'status' => false], $code);
  }

  public function result($data, $code = parent::HTTP_OK)
  {
      return $this->response(['data' => $data, 'status' => true], $code);
  }

  /* --------------------------------------------------------------
    * RECORDS RENDERING
    * Fetch limited records with common fields and pagination
    * 
    * ------------------------------------------------------------ */
  public function index_get()
  {
    $params = $this->get();

    $limit = $this->get('limit') ?? 20;
    $offset = (($this->get('page') ?? 1) - 1) * $limit;

    $search = array_filter($params, function ($key) {
    return $key !== 'limit' && $key !== 'page';
    }, ARRAY_FILTER_USE_KEY);

    $products = $this->{$this->default_model}->get_limit($search, $limit, $offset);
    $recordsTotal = $this->{$this->default_model}->count_all();
    $recordsFiltered = $this->{$this->default_model}->count_by_search($search);

    $response = [
      'data' => $products, 
      'recordsTotal' => $recordsTotal, 
      'recordsFiltered' => $recordsFiltered,
      'pageTotal' => ceil($recordsFiltered / $limit),
      'page' => $this->get('page') ?? 1,
      'status' => true
    ];
    
    return $this->response($response);
  }

  public function datatables_get()
  {
    $search = json_decode($this->get('search')['value']) ?? null ; $data = array();
    $limit = $this->get('length') ?? 20;
    $offset = $this->get('start') ?? 1;

    $rows = $this->{$this->default_model}->get_limit($search, $limit, $offset);
    $recordsTotal = $this->{$this->default_model}->count_all();
    $recordsFiltered = $this->{$this->default_model}->count_by_search($search);

    foreach ($rows as $row)
    {
      $sub_array = array();
      $sub_array[] = ++$offset;
      foreach ($this->fields as $field)
      {
        $sub_array[] = $row[$field];
      }
      $sub_array[] = "<a class=\"btn btn-success btn-sm\" href=\"users/{$row['user_id']}/edit\"><span class=\"fa fa-edit mr-2\" aria-hidden=\"true\">{$this->lang->line('edit')}</span></a>";
      $data[] = $sub_array; 
    }

    $response = [
      'draw' => intval($this->post('draw')),
      'data' => $data, 
      'recordsTotal' => $recordsTotal, 
      'recordsFiltered' => $recordsFiltered,
      'pageTotal' => ceil($recordsFiltered / $limit),
      'page' => $this->get('page') ?? 1,
      'status' => true,
    ];
    
    return $this->response($response);
  }
}