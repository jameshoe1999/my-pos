<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller as REST;

/**
 * Custom Exceptions Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Exceptions
 * 
 * @link		https://codeigniter.com/user_guide/libraries/exceptions.html
 */
class MY_Exceptions extends CI_Exceptions 
{
  /**
   * 404 Error Handler
   *
   * @uses	CI_Exceptions::show_error()
   *
   * @param	string	$page		Page URI
   * @param 	bool	$log_error	Whether to log the error
   * @return	void
   */
  public function show_error($heading = NULL, $message = '', $template = 'error_general', $status_code = 500)
  {
    if (!$message) {
      switch($status_code)
      {
        case REST::HTTP_BAD_REQUEST:
          $message = 'Invalid request, please try again.';
        break;
        case REST::HTTP_UNAUTHORIZED:
          $message = 'Unauthorized request, please try again.';
        break;
        case REST::HTTP_FORBIDDEN:
          $message = 'Forbidden request, please try again.';
        break;
        case REST::HTTP_NOT_FOUND:
          $message = 'Request not found, please try again.';
        break;
        default:
          $message = 'Internal server error, please contact system admin.';
      }
    }

    header('Content-Type: application/json');
    header("HTTP/1.0 $status_code");
    echo json_encode(['error' => $message, 'status' => false]);
    exit(1);
  }
}