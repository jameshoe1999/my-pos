<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends MY_Controller {

	public function __construct()
	{
    $this->default_model = 'products';
    parent::__construct();
    $this->load->model('ProductModel', 'products');
    $this->load->model('VariantModel', 'variants');
    if ($this->require_auth && $this->user['user_role'] > 1) {
      array_push($this->variants->hidden_attributes, "product_cost");
    }
  }

  public function show_get($id)
  {
    $this->products->_table = 'products_v'; // return formatted views 
    $product = $this->products->with('variants')->get($id);
    if (!$product)
      return $this->return_404();
    return $this->return_success($product);
  }

  public function create_post()
  {
    $insert = $this->post();
    $this->form_validation->set_data($insert);
    
    // set default value for certain fields
    $insert['effective_date_start'] = $insert['effective_date_start'] ?? date('Y-m-d');
    $insert['effective_date_start'] = $insert['effective_date_start'] ?? '2100-12-31';
    $insert['created_by'] = $this->user['user_name'];
    
    $product_id = $this->products->insert($insert);

    if ($product_id) {
      return $this->response(['message' => 'Created!', 'link' => base_url("api/products/$product_id"), 'status' => true]);
    } else {
      return $this->response(['error' => $this->products->validate_errors, 'status' => false], parent::HTTP_BAD_REQUEST);
    }
  }

  public function update_put($id)
  {
    $update = $this->put();
    $this->form_validation->set_data($update);
    if ($this->products->update($id, $update)) 
      return $this->response(['message' => 'Updated successfully.', 'status' => true]);
    else
      return $this->response(['error' => $this->products->validate_errors, 'status' => false], parent::HTTP_BAD_REQUEST);
  }

  public function destroy_delete($id)
  {
    $delete = $this->products->delete($id);
    if ($delete) {
      $this->products->update($id, array('deleted_by' => $this->user['user_name']));
      return $this->response(['message' => 'Product removed successfully.', 'status' => true]);
    }
    return $this->response(['message' => 'Internal server error!', 'status' => false], parent::HTTP_INTERNAL_SERVER_ERROR);
  }
}
