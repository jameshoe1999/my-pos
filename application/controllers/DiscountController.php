<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DiscountController extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->load->model('InvoiceModel', 'invoices');
    $this->load->model('DiscountModel', 'discounts');
  }

  public function create_post($invoice_id)
    {
      $inputs = $this->post();
      $invoice = $this->invoices->get($invoice_id);

      if ($invoice->invoice_status == 0) {
        show_error("The invoice is ineligible for discount.");
      }

      // type casting to prevent calculation error
      $sub_total = (float) $invoice->invoice_sub_total;
      $discount = (float) calculate_discount($sub_total, $inputs['discount_amount'], $inputs['discount_type']);
      $b_rounded_amount = $sub_total - $discount; //before rounded amount
      $a_rounded_amount = calculate_rounding($b_rounded_amount); //after rounded amount
      $rounding = (float) number_format($a_rounded_amount - $b_rounded_amount, 2);
      
      $new_discount = [
        "discount_invoice" => $invoice_id,
        "discount_type" => $inputs['discount_type'],
        "discount_amount" => $discount,
        "discount_rounding" => $rounding,
        "discount_remark" => $inputs['discount_remark'],
        "created_by" => $this->user['user_name']
      ];

      $invoice_update = [
        "invoice_discount" => $discount,
        "invoice_rounding" => $rounding,
        "invoice_amount" => $a_rounded_amount,
        "updated_by" => $this->user['user_name']
      ];

      $this->discounts->insert($new_discount);
      $this->invoices->update_existing_invoice($invoice_id, $invoice_update);

      return $this->return_success();
    }
}