<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ColorController extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('ColorModel', 'colors');
  }

  public function create_post()
  {
    $inputs = $this->post();
    $this->form_validation->set_data($inputs);

    $id = $this->colors->insert($inputs);

    if (!$id) { show_error($this->form_validation->error_array(), parent::HTTP_BAD_REQUEST); }

    return $this->return_success();
  }
}