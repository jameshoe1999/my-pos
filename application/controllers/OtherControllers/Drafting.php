<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Drafting extends MY_Controller 
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('draft_model', 'drafts');
  }

  public function create_post()
  {
    $records = $this->post('records');
    $type = $this->post('type');
    $write_in = '';
    $path = "application/cache/{$type}s/{$type}-{$this->user['user_name']}.txt";

    foreach ($records as $record)
    {
      if (!$record) { continue; }
      $data = '';
      $remark = $record['remark'] ?? '';
      $data = "{$record['item']}\t{$record['qty']}\t{$record['cost']}\t{$remark}\n";
      $write_in .= $data;
    }

    if (!write_file($path, $data, 'w')) {
      throw new Exception("Failed to save as draft.", parent::HTTP_INTERNAL_SERVER_ERROR);
    } else {
      $insert = array('user_id', $this->user['user_id'], 'draft_type' => $type, 'file' => $path);
      $this->drafts->insert($insert);
    }

    return $this->return_success(NULL, parent::HTTP_ACCEPTED);
  }
}