<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryController extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('OtherModels/CategoryModel', 'categories');
  }

  public function create_post()
  {
    $input = $this->post();
    $input['created_by'] = $this->user['user_name'];
    
    if ($this->categories->insert($input))
      return $this->response(['message' => 'Created successfully.', 'status' => true]);
    else
      return $this->response(['error' => $this->form_validation->error_array(), 'status' => false], parent::HTTP_INTERNAL_SERVER_ERROR);
  }
}