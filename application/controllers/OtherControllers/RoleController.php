<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoleController extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('OtherModels/RoleModel', 'roles');
  }

  public function create_post()
  {
    $input = $this->post();
    $input['created_by'] = $this->user['user_name'];
    
    if ($this->roles->insert($input))
      return $this->response(['message' => 'Created successfully.']);
    else
      return $this->response(['error' => $this->form_validation->error_array()], parent::HTTP_INTERNAL_SERVER_ERROR);
  }
}