<?php

DEFINED('BASEPATH') or exit('No direct script allowed!');

/**
 * Sales Controller meant for dealer to create new orders
 * These orders will not affect the product stock nor the
 * operation.
 */
class SalesController extends MY_Controller
{
  function __construct()
  {
    $this->default_model = 'sales';
    parent::__construct();
    $this->load->model('SalesModel', 'sales');
    $this->load->model('SalesDetailModel', 'details');
    $this->load->model('VariantModel', 'variants');
  }

  public function create_post()
  {
    $this->benchmark->mark('start');
    $sales = $this->post('sales');
    $items = $this->post('items');

    if (is_array($items) && !count($items)) {
      return $this->response(['error'=> 'Must contain at least one item!', 'status' => false], parent::HTTP_BAD_REQUEST);
    } elseif (!$this->sales->validate($sales)) {
      show_error($this->sales->validate_errors);
    }

    $sales['created_by'] = $this->user['user_name'];
    
    $id = $this->sales->insert($sales);

    if (!$id) { 
      show_error($this->form_validation->error_array(), parent::HTTP_BAD_REQUEST); 
    }

    foreach ($items as &$item) {
      $product = $this->variants->get($item['product_id']);
      if (! $product) {
        show_error('Invalid product.', parent::HTTP_BAD_REQUEST);
      }
      $item['sale_id'] = $id;
      $item['sale_cost'] =  $item['sale_cost'] ?? $product['product_cost'];
      $item['sale_price'] =  $item['sale_price'] ?? $product['product_price'];  
      $item['sale_sub_total'] =  (float) $item['sale_price'] * (float) $item['sale_qty'];
      if (!$this->details->validate($item)) {
        $this->return_error($this->form_validation->error_array(), parent::HTTP_BAD_REQUEST);
        return;
      }
    }

    if (!$this->details->insert_many($items)) {
      show_error('');
    }

    return $this->return_success(NULL, parent::HTTP_OK, CREATE);
  }
}