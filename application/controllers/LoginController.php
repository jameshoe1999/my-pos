<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends MY_Controller {

	public function __construct()
  {
    $this->require_auth = false;
    parent::__construct();
    $this->users->hidden_attributes = NULL;
  }

  public function authenticate($username, $password)
  {
    $hashed = hash('sha1', $password);

    $user = $this->users->get_by('user_name', $username);

    if (!$user) { return FALSE; }

    if ($user['user_password'] === $hashed) {
      unset($user['user_password']);
      $this->user = $user;
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function generateToken($username)
  {
    return AUTHORIZATION::generateToken(['username' => $username]);
  }

	public function index_post()
	{
    $credential = $this->post();
    if (! $this->authenticate($credential['username'], $credential['password'])) {
      return $this->response(['error' => 'Invalid credential.', 'status' => false], 400);
    }
    $token = $this->generateToken($credential['username']);

    $response = [
      'message' => "Welcome, {$credential['username']}!",
      'user' => $this->user,
      'token' => $token, 
      'status' => true
    ];

    return $this->response($response);
	}
}
