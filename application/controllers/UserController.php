<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends MY_Controller
{
  public $fields = array('user_name', 'full_name', 'user_role', 'user_designation');
  public function __construct()
  {
    $this->default_model = 'users';
    $this->require_auth = true;
    parent::__construct();
  }

  public function datatables_get()
  {
    $this->users->_table = 'users_v';
    parent::datatables_get();
  }

  public function export_get()
  {
    $this->users->_table = 'users_v';
    $users = $this->users->get_all();

    foreach ($users as $user)
    {
      $row[] = "MASTER;{$user['user_id']};300-000;\"{$user['user_first_name']}\";;----;{$user['city']};;;\"60 DAYS\";5000;----;F;;;;;\"{$user['user_last_name']}\";;;;;
      DETAIL;{$user['user_id']};BILLING;\"{$user['address_1']}\";\"{$user['address_2']}\";;;;{$user['mobile_no']};;;;\"{$user['email']}\";";
    }

    $result = implode("\n", $row);

    print_r($result); return 0;
  }
}