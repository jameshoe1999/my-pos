<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VariantController extends MY_Controller {

	public function __construct()
	{
    $this->default_model = 'variants';
    parent::__construct();
    $this->load->model('VariantModel', 'variants');
    if ($this->user['user_role'] > 1) {
      array_push($this->variants->hidden_attributes, "variant_cost");
    }
  }

  public function show_get($id)
  {
    $variant = $this->variants->get($id);
    if (!$variant)
      return $this->response(['error' => 'No record found.', 'status' => false], parent::HTTP_NOT_FOUND);
    return $this->response(['data' => $variant, 'status' => true]);
  }

  public function create_post()
  {
    $insert = $this->post();
    $this->form_validation->set_data($insert);

    $product_qty = $insert['product_qty'] ?? 0;
    
    // set default value for certain fields
    $insert['initial_qty'] = $product_qty;
    $insert['current_qty'] = $product_qty;
    $insert['effective_date_start'] = $insert['effective_date_start'] ?? date('Y-m-d');
    $insert['effective_date_start'] = $insert['effective_date_start'] ?? '2100-12-31';
    $insert['created_by'] = $this->user['user_name'];

    // unset product qty to prevent unknown column
    unset($insert['product_qty']);
    
    $id = $this->variants->insert($insert);

    if ($id) {
      return $this->response(['message' => 'Created!', 'link' => base_url("api/products/$id"), 'status' => true]);
    } else {
      return $this->response(['error' => $this->variants->validate_errors, 'status' => false], parent::HTTP_BAD_REQUEST);
    }
  }
}