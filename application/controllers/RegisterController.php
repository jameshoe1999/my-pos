<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterController extends MY_Controller {
  public function __construct()
  {
    $this->require_auth = false;
    parent::__construct();
  }

  public function index_post()
  {
    $input = $this->post();
    if (!$this->validate_form($input)) {
      return $this->response(['error' => $this->form_validation->error_array(), 'status' => false], parent::HTTP_BAD_REQUEST);
    }

    $input['user_password'] = hash('sha1', $input['user_password']);
    unset($input['password_conf']);

    if (!$this->users->insert($input))
      return $this->response(['error' => 'Internal server error!', 'status' => false], parent::HTTP_INTERNAL_SERVER_ERROR);

    return $this->response(['message'=>'Registered successfully!', 'status' => true], parent::HTTP_OK);
  }

  public function validate_form($data)
  {
    $this->form_validation->set_data($data);

    $rules = array(
      ['field' => 'user_name', 'label' => 'Username', 'rules' => 'trim|required|is_unique[users.user_name]|min_length[5]|max_length[50]'],
      ['field' => 'user_first_name', 'label' => 'First Name', 'rules' => 'trim|required|min_length[5]|max_length[30]'],
      ['field' => 'user_last_name', 'label' => 'Last Name', 'rules' => 'trim|required|max_length[30]'],
      ['field' => 'user_password', 'label' => 'Password', 'rules' => 'trim|required|min_length[8]|max_length[20]'],
      ['field' => 'password_conf', 'label' => 'Password Confirmation', 'trim|required|matches[user_password]'],
      ['field' => 'user_email', 'label' => 'Email', 'rules' => 'trim|required|valid_email'],
      ['field' => 'user_mobile_no', 'label' => 'Mobile No.', 'rules' => 'trim|required|min_length[10]|max_length[11]'],
      ['field' => 'user_designation', 'label' => 'Designation', 'rules' => 'trim|max_length[20]'],
    );

    $this->form_validation->set_rules($rules);
    return $this->form_validation->run();
  }
}