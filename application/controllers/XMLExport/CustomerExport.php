<?php

class CustomerExport extends MY_Controller
{
  public function __construct()
  {
    $this->require_auth = false;
    parent::__construct();
    $this->load->helper('file');
    $this->load->helper('xml');
    $this->load->dbutil();
  }
  
  public function index_get()
  {
    $users = array();
    $users = $this->db->query('SELECT * FROM users;');

    $config = array (
      'root'    => 'root',
      'element' => 'element',
      'newline' => "\n",
      'tab'     => "\t"
    );

    $result = $this->dbutil->xml_from_result($users, $config);
    
    echo write_file("./resources/reports/customers/customer-info.xml", $result) ? 'File written!' : 'Unable to write file.';
    return;
  }
}