<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceController extends MY_Controller {

	public function __construct()
	{
    parent::__construct();
    $this->load->model('InvoiceModel', 'invoices');
    $this->load->model('InvoiceDetailModel', 'details');
    $this->load->model('VariantModel', 'variants');
    $this->load->model('ProductTransactionModel', 'transactions');
  }

  public function index_get()
  {
    $this->invoices->_table = 'invoices_v';
    $invoices = $this->invoices->get_all();
    return $this->result($invoices);
  }

  public function show_get($id)
  {
    $invoice = $this->invoices->with('items')->get($id);
    return $this->return_success($invoice);
  }

  public function create_post()
  {
    $amount = 0;
    $input = $this->post('invoice') ?? NULL;
    $items = $this->post('items') ?? NULL;

    if (is_array($items) && !count($items)) {
      return $this->response(['error'=> 'Must contain at least one item!', 'status' => false], parent::HTTP_BAD_REQUEST);
    } elseif (!$this->invoices->validate($input)) {
      show_error($this->invoices->validate_errors);
    }

    // loop out the given list to ensure there's enough stock, calculate subtotal and amount
    foreach ($items as &$item)
    {
      if (!$this->details->validate($item))
        show_error($this->details->validate_errors, parent::HTTP_BAD_REQUEST);
      
      $product = $this->variants->get($item['product_id']);

      if (!$product) { 
        return $this->return_404(); 
      } elseif ($product['current_qty'] < $item['item_qty']) {
        show_error("{$product['product_code']} has insufficient stock.", parent::HTTP_BAD_REQUEST);
      }

      $item['item_price'] = $item['item_price'] ?? (float) $product['product_price'];
      $item['item_cost'] = $item['item_cost'] ?? (float) $product['product_cost'];
      $item['item_sub_total'] = (int) $item['item_qty'] * (float) $item['item_price'] ?? (float) $product['product_price'];
      $item['created_by'] = $this->user['user_name'];
      $amount += $item['item_sub_total'];
    }

    $input['created_by'] = $this->user['user_name']; //create signature
    if ($id = $this->invoices->insert($input)) {
      $invoice_code = code_generator($id, 'INV');
      update_column($id, $items, 'invoice_id'); //insert invoice id to every item row
      $this->invoices->update($id, ['invoice_code' => $invoice_code, 'invoice_amount' => $amount], TRUE);
      $this->details->insert_many($items, TRUE);
      array_walk($items, function($obj) use ($invoice_code) {
        $transaction_id = $this->transactions->insert(array(
          'product_id' => $obj['product_id'],
          'transaction_qty' => $obj['item_qty'],
          'transaction_remark' => "Invoice: $invoice_code",
          'created_by' => $this->user['user_name'],
        ));
        $this->transactions->update($transaction_id, array('transaction_code' => code_generator($transaction_id, 'TRS')));
        $this->variants->reduce_stock($obj['product_id'], $obj['item_qty']);
      });
    } else {
      show_error(NULL, parent::HTTP_BAD_REQUEST);
    }
    return $this->response([
      'message' => 'Created successfully.', 
      'link' => base_url("api/invoices/$id"),
      'status' => true
    ], parent::HTTP_ACCEPTED);
  }

  public function update_put($id)
  {
    $update = $this->put();
    $update['updated_by'] = $this->user['user_name'];
    $this->form_validation->set_data($update);
    
    if (!$this->invoices->update($id, $update, true)) {
      $errors = $this->form_validation->error_array() ?? NULL;
      show_error($errors);
    }

    return $this->return_success(NULL, parent::HTTP_OK, UPDATE);
  }
}