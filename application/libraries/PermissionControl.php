<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PermissionControl
{
  public function authorization()
  {
    return $this->users->get_all();
  }
}