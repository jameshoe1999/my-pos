<?php

if (! function_exists('code_generator')) {

  /**
   * Reference Code Generator
   * 
   * Generate adn return reference code based on the given prefix
   * and the record ID, append to the current year and month 
   * 
   * @param integer $count
   * 
   * @return string
   */
  function code_generator($id, $prefix)
  {
    return $prefix.date('ym').str_pad($id, 5, "0", STR_PAD_LEFT);
  }

}

if (! function_exists('update_column')) {

  /**
   * Edit items column
   *  
   * @param integer $id
   * @param Array $items
   * @param string $column_name
   * 
   * @return void
   */
  function update_column($id, &$items, $column_name)
  {
    foreach ($items as &$item)
    {
      $item[$column_name] = $id;
    }
  }
}

if (!function_exists('calculate_amount')) {
  /**
   * Calculate the final amount and simple rounding
   */
  function calculate_amount($sub_total, $discount)
  {
    $amount_before_round = $sub_total - $discount;
    $amount_after_round = round($amount_before_round * 2, 1) / 2;
    
    $rounding = $amount_before_round - $amount_after_round;

    return [$amount_after_round, $rounding];
  }
}

if (!function_exists('calculate_discount')) {
  /**
   * Calculate discount amount
   * Type refer to either count by percentage or actual amount
   * For percentage type, provide the actual percentage rather
   * than decimal
   * 
   * @param float $sub_total
   * @param float $discount
   * @param int $type
   * 
   * @return float $discounted_amount 
   */
  function calculate_discount($sub_total, $discount, $type = 1)
  {
    $discounted_amount = 0;
    switch($type)
    {
      case 1: //discount by percentage
        if ($discount > 1)
          $discounted_amount = $sub_total - $sub_total * round($discount/100, 2);
        elseif ($discount < 1 && $discount > 0)
          $discounted_amount = $sub_total * round($discount, 2);
        elseif ($discount < 0)
          $discounted_amount = $sub_total * round(abs($discount), 2);
        else
          $discounted_amount = $sub_total;
      break;
      case 2:
        $discounted_amount = $sub_total = $discount;
      break;
      default:
    }

    return $discounted_amount;
  }
}

if (!function_exists('calculate_rounding')) {
  /**
   * Calculate discount amount
   * Type refer to either count by percentage or actual amount
   * For percentage type, provide the actual percentage rather
   * than decimal
   * 
   * @param float $sub_total
   * @param float $discount
   * @param int $type
   * 
   * @return float $discounted_amount 
   */
  function calculate_rounding($sub_total)
  {
    return round($sub_total * 2, 1) / 2 ;
  }
}