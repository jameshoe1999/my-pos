<?php

class VariantModel extends MY_Model
{
  public $_table = 'product_variants';
  public $primary_key = 'id';
  public $_fields = array('product_code', 'product_description');

  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');
  public $protected_attributes = array('id');

  public $belongs_to = array('parent' => array('model' => 'ProductModel', 'primary_key' => 'product_id'));

  public $validate = array(
    array('field' => 'product_id', 'label' => 'product ID', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'product_code', 'label' => 'product code', 'rules' => 'is_unique[product_variants.product_code]|min_length[5]'),
    array('field' => 'product_color', 'label' => 'product color', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'product_type', 'label' => 'product type', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'product_cost', 'label' => 'product cost', 'rules' => 'numeric|greater_than[0]'),
    array('field' => 'product_price', 'label' => 'product price', 'rules' => 'numeric|greater_than[0]'),
    array('field' => 'product_qty', 'label' => 'product quantity', 'rules' => 'numeric|greater_than[0]'),
    array('field' => 'product_description', 'label' => 'Description', 'rules' => 'max_length[100]'),
    array('field' => 'effective_date_start', 'label' => 'effective start', 'rules' => 'regex_match[/^\d{4}\-\d{2}\-\d{2}$/]'),
    array('field' => 'effective_date_end', 'label' => 'effective end', 'rules' => 'regex_match[/^\d{4}\-\d{2}\-\d{2}$/]'),
  );

  public function __construct()
  {
    parent::__construct();
  }

  public function reduce_stock($id, $qty)
  {
    if (!$qty) { return false; }
    $query = "UPDATE product_variants SET current_qty = current_qty - $qty WHERE id = $id";

    $result = $this->_database->query($query);
    return $result;
  }
}