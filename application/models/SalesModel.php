<?php

class SalesModel extends MY_Model
{
  public $primary_key = 'sale_id';
  public $_table = 'point_of_sales';

  public $_fields = array('sale_code', 'sale_remark', 'created_by');

  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');
  public $protected_attributes = array('sale_id', 'sale_code');

  public $has_many = array('items' => array('model' => 'SalesDetailModel', 'primary_key' => 'sale_id'));

  public $validate = array(
    array('field' => 'customer_id', 'label' => 'customer', 'rules' => 'required|integer|greater_than[0]'),
    array('field' => 'sale_remark', 'label' => 'remark', 'rules' => 'required|max_length[100]'),
  );

  public function __construct()
  {
    parent::__construct();
  }
}