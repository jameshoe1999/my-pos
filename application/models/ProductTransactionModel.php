<?php

class ProductTransactionModel extends MY_Model
{
  // public $_database = 'my_pos_system';
  public $_table = 'product_transactions';
  public $primary_key = 'transaction_id';
  public $_fields = array('transaction_code', 'transaction_remark');

  public $protected_attributes = array('transaction_id');
  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');

  public $belongs_to = array('products' => array('model' => 'ProductModel', 'primary_key' => 'product_id'));

  public $validate = array(
    array('field' => 'transaction_qty', 'label' => 'quantity', 'rules' => 'required|integer|is_natural_no_zero'),
    array('field' => 'transaction_remark', 'label' => 'remark', 'rules' => 'required|max_length[100]'),
    array('field' => 'transaction_type', 'label' => 'type', 'rules' => 'required|in_list[1,2,3]'),
  );

  public function __construct()
  {
    parent::__construct();
  }
}