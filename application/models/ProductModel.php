<?php

class ProductModel extends MY_Model
{
  // public $_database = 'my_pos_system';
  public $_table = 'products';
  public $primary_key = 'product_id';
  public $_fields = array('product_name', 'product_code', 'product_description');

  public $protected_attributes = array('product_id');
  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');

  public $has_many = array('variants' => array('model' => 'VariantModel', 'primary_key' => 'product_id'));

  public $validate = array(
    array('field' => 'product_name', 'label' => 'product name', 'rules' => 'is_unique[products.product_name]|min_length[5]'),
    array('field' => 'product_code', 'label' => 'product code', 'rules' => 'is_unique[products.product_code]|min_length[5]'),
    array('field' => 'product_description', 'label' => 'description', 'rules' => 'max_length[100]'),
    array('field' => 'product_brand', 'label' => 'product brand', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'product_category', 'label' => 'product category', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'effective_date_start', 'label' => 'effective start', 'rules' => 'regex_match[/^\d{4}\-\d{2}\-\d{2}$/]'),
    array('field' => 'effective_date_end', 'label' => 'effective end', 'rules' => 'regex_match[/^\d{4}\-\d{2}\-\d{2}$/]'),
  );

  public function __construct()
  {
    parent::__construct();
  }
}