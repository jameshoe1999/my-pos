<?php

class InvoiceModel extends MY_Model
{
  public $primary_key = 'invoice_id';
  public $_table = 'invoices';

  public $_fields = array('invoice_code', 'invoice_remark');

  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');
  public $protected_attributes = array('invoice_id', 'invoice_code');

  public $has_many = array('items' => array('model' => 'InvoiceDetailModel', 'primary_key' => 'invoice_id'));

  public $validate = array(
    array('field' => 'user_id', 'label' => 'user', 'rules' => 'required|integer|greater_than[0]'),
    array('field' => 'invoice_remark', 'label' => 'remark', 'rules' => 'required|max_length[100]'),
  );

  public function __construct()
  {
    parent::__construct();
  }
}