<?php

class PermissionModel extends MY_Model
{
  // public $_database = 'my_pos_system';
  public $_table = 'permissions_table';
  public $primary_key = 'id';
  public $_fields = array('permission_name');

  public $hidden_attributes = array('created_at', 'deleted_at', 'updated_at');

  public function __construct()
  {
    parent::__construct();
  }
}