<?php

class AdjustmentModel extends MY_Model
{
  public $primary_key = 'adjustment_id';
  public $_table = 'product_adjustments';

  public $_fields = array('adjustment_code', 'adjustment_remark');

  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');
  public $protected_attributes = array('adjustment_id', 'adjustment_code');

  public $has_many = array('items' => array('model' => 'adjustmentDetailModel', 'primary_key' => 'adjustment_id'));

  public $validate = array(
    array('field' => 'user_id', 'label' => 'user', 'rules' => 'required|integer|greater_than[0]'),
    array('field' => 'adjustment_remark', 'label' => 'remark', 'rules' => 'required|max_length[100]'),
  );

  public function __construct()
  {
    parent::__construct();
  }
}