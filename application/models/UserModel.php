<?php

class UserModel extends MY_Model
{
  public $_table = 'users';
  public $primary_key = 'user_id';
  public $_fields = array('user_name', 'user_first_name', 'user_last_name', 'user_designation');

  public $hidden_attributes = array('user_password');

  public $return_type = 'array';

  public function __construct()
  {
    parent::__construct();
  }
}