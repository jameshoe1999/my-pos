<?php

class SalesDetailModel extends MY_Model
{
  public $_table = 'sale_records';
  public $primary_key = 'id';
  public $_fields = array('sale_code', 'sale_remark');

  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');
  public $protected_attributes = array('record_id');

  public $belongs_to = array('parent' => array('model' => 'SalesModel', 'primary_key' => 'sale_id'));

  public $validate = array(
    array('field' => 'sale_id', 'label' => 'sale ID', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'product_id', 'label' => 'product ID', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'sale_qty', 'label' => 'sale quantity', 'rules' => 'integer'),
    array('field' => 'sale_cost', 'label' => 'sale cost', 'rules' => 'numeric'),
    array('field' => 'sale_price', 'label' => 'sale price', 'rules' => 'numeric'),
    array('field' => 'sale_remark', 'label' => 'remark', 'rules' => 'max_length[50]'),
  );

  public function __construct()
  {
    parent::__construct();
  }
}