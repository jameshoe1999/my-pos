<?php

class InvoiceDetailModel extends MY_Model
{
  public $_table = 'invoice_details';
  public $primary_key = 'id';
  public $_fields = array('invoice_remark');

  public $hidden_attributes = array('created_at', 'deleted_at', 'deleted_by', 'updated_at');
  public $protected_attributes = array('id');

  public $belongs_to = array('parent' => array('model' => 'InvoiceModel', 'primary_key' => 'invoice_id'));

  public $validate = array(
    array('field' => 'invoice_id', 'label' => 'invoice ID', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'product_id', 'label' => 'product ID', 'rules' => 'integer|greater_than[0]'),
    array('field' => 'item_qty', 'label' => 'item quantity', 'rules' => 'integer'),
    array('field' => 'item_cost', 'label' => 'item cost', 'rules' => 'numeric'),
    array('field' => 'item_price', 'label' => 'item price', 'rules' => 'numeric'),
    array('field' => 'invoice_remark', 'label' => 'remark', 'rules' => 'max_length[50]'),
  );

  public function __construct()
  {
    parent::__construct();
  }
}