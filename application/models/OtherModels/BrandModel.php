<?php

class BrandModel extends MY_Model
{
  public $_table = 'product_brands';
  public $primary_key = 'brand_id';

  public $protected_attributes = array('brand_id');

  public $validate = array(
    ['field' => 'brand_name', 'label' => 'name', 'rules' => 'is_unique[product_brands.brand_name]']
  );

  public function __construct()
  {
    parent::__construct();
  }
}