<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ColorModel extends MY_Model
{
  public $_table = 'product_colors';
  public $primary_key = 'color_id';

  public $validate = array(
    ['field' => 'color_name', 'label' => 'name', 'rules' => 'required|is_unique[product_colors.color_name]'],
    ['field' => 'color_code', 'label' => 'hex code', 'rules' => 'required|is_unique[product_colors.color_code]|regex_match[/#[a-zA-Z0-9]{6}$/]'],
    ['field' => 'color_description', 'label' => 'description', 'rules' => 'max_length[100]']
  );

  public function __construct()
  {
    parent::__construct();
  }
}