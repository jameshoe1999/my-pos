<?php

class RoleModel extends MY_Model
{
  public $_table = 'user_roles';
  public $primary_key = 'role_id';

  public $validate = array(
    ['field' => 'role_name', 'label' => 'name', 'rules' => 'is_unique[user_roles.role_name]']
  );

  public function __construct()
  {
    parent::__construct();
  }
}