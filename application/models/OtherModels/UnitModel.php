<?php

class UnitModel extends MY_Model
{
  public $_table = 'product_uom';
  public $primary_key = 'unit_id';

  public $protected_attributes = array('unit_id');

  public $validate = array(
    ['field' => 'unit_name', 'label' => 'name', 'rules' => 'is_unique[product_uom.unit_code]']
  );

  public function __construct()
  {
    parent::__construct();
  }
}