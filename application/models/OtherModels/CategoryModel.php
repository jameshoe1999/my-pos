<?php

class categoryModel extends MY_Model
{
  public $_table = 'product_categories';
  public $primary_key = 'category_id';

  public $protected_attributes = array('category_id');

  public $validate = array(
    ['field' => 'category_name', 'label' => 'name', 'rules' => 'is_unique[product_categories.category_name]']
  );

  public function __construct()
  {
    parent::__construct();
  }
}