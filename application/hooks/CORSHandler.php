<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CORSHandler
{
  /**
   * To handle CORS request
   * Only response to OPTIONS request
   */
  public function cors_handler()
  {
    $method = $_SERVER['REQUEST_METHOD'];
    if ($method == "OPTIONS") {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Headers: Origin, Content-Type, Accept, Access-Control-Request-Method, Authorization");
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
      header("HTTP/1.1 200 OK");
      exit();
    }
  }
}